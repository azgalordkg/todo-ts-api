const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();

const tasks = require("./app/tasks");

app.use(express.json());
app.use(express.static("public"));
app.use(cors());

const port = 8000;

mongoose.connect("mongodb://localhost/tasks", {useNewUrlParser: true}).then(() => {
  app.use("/tasks", tasks);
  
  app.listen(port, () => {
    console.log(`Server started on http://localhost:${port}`);
  });
});
