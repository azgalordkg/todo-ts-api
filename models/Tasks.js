const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TasksSchema = new Schema({
  title: {
    type: String, required: true
  },
  description: {
    type: String, required: true
  },
  status: {
    type: Boolean, required: true
  }
});

const Tasks = mongoose.model("Tasks", TasksSchema);
module.exports = Tasks;
