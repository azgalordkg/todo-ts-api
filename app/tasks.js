const express = require("express");
const Tasks = require("../models/Tasks");

const router = express.Router();

router.get("/", (req, res) => {
  Tasks.find()
    .then(tasks => res.send(tasks))
    .catch(() => res.status(500).send({error: "Server error."}));
});

router.get("/:id", (req, res) => {
  Tasks.findById(req.params.id)
    .then(result => {
      if (result) return res.send(result);
      res.status(404).send({error: "Task not fount"});
    })
    .catch(() => res.status(500).send({error: "Server error."}));
});

router.post("/", (req, res) => {
  const tasks = new Tasks(req.body);
  
  tasks.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.put("/:id", (req, res) => {
  const task = new Tasks(req.body);
  
  Tasks.updateOne({_id: req.params.id}, task)
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete("/:id", (req, res) => {
  Tasks.findOneAndRemove({_id: req.params.id})
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

module.exports = router;
